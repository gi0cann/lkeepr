<?php
require('includes/members.php');
$user = new Membership;
session_start();
  if ( $_SERVER["REQUEST_METHOD"] == "POST" ) {
    if ( isset($_POST['username']) && isset($_POST['password'])) {
      $login_result = $user->signIn($_POST['username'], $_POST['password']);
      if ( $login_result['success'] ) {
        $_SESSION['userinfo'] = $login_result;
      }
    }
  }

?>
    <?php include('includes/nav.php'); ?>
    <div class="container">
      <?php
      if (isset($login_result['success'])) {
        if ( !$login_result['success'] ) { ?>
          <div class="row error"><h2>Invalid login info<h2></div>
       <?php }
      } ?>

      <div class="jumbotron">
        <div class="container">
          <h1>LKeepr</h1>
          <p>Welcome to LKeepr a simple list sharing web application.</p>
          <p><a href="#" class="btn btn-primary btn-lg">Learn more</a>
        </div>
      </div>
    </div>
  </body>
</html>
