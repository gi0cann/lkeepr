<!doctype html>
<html lang="en">
  <head>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/signup.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <title>LKeepr Sign Up Page</title>
  </head>

  <body>
    <div class="container">
      <?php
      if ($_SERVER['REQUEST_METHOD'] == "POST") {
        require('includes/members.php');
        $signup = new Membership;
        $fullname = $_POST["fullname"];
        $email = $_POST["email"];
        $password = $_POST["password"];
        $signup->signUp($email, $password, $fullname);
        if ($signup) {
          echo <<<SUCCESS
                  <h1>Sign Up successfull</h1>
                  <p>Redirecting to home page...</p>
SUCCESS;
          header('Refresh: 5; URL=index.php');
        } else {
          echo "<h1>Sign Up failed</h1>";
        }


      } else {
      ?>
      <form action="" class="form-signup" method="POST">
        <h2 class="form-signup-heading">Sign up here..</h2>
        <input type="text" class="form-control" placeholder="Fullname" name="fullname">
        <input type="text" class="form-control" placeholder="Email address" name="email">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Sign up</button>
      </form>
      <?php } ?>
    </div>
  </body>

</html>