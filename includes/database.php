<?php
/**
 * database.php
 *
 * Author: Gionne Cannister: 10-20-2013
 *
 * This file contains database connection class for lkeepr
 */

require('dbinfo.inc');

class Database {

  public function __construct() {
    $this->dbhost = DB_HOST;
    $this->dbuser = DB_USER;
    $this->dbpass = DB_PASS;
    $this->dbase = DB_DBASE;
  }

  public function echoDbInfo() {
    echo <<<DB
    <p>$this->dbhost</p>
    <p>$this->dbuser</p>
    <p>$this->dbpass</p>
    <p>$this->dbase</p>
DB;
  }

  public function connectDB() {
    $this->dsn = "mysql:dbname=$this->dbase;host=$this->dbhost";
    try {
      $this->conn = new PDO($this->dsn, $this->dbuser, $this->dbpass);
      $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      return $this->conn;
    } catch (PDOExecption $e) {
      return false;
    }
  }
}

// unit testing
// $lkdbase = new Database;
// $lkdbase->echoDbInfo();
// $lkdbase->connectDB()

?>