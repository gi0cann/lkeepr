<?php
/**
 * members.php
 *
 * Author: Gionne Cannister: 10-20-2013
 *
 * This file contains database connection class for lkeepr
 */

require('database.php');

class Membership extends Database {

  public function __construct() {
    parent::__construct();
    $this->dbconn = $this->connectDB();
  }

  public function checkEmail($email) {
    $email_pattern = "/^[^@]*@[^@]*\.[^@]*$/";
    return preg_match($email_pattern, $email);
  }

  public function checkPassword($pw) {
    $pw = trim($pw);
    return strlen($pw) >= 6 and strlen($pw) < 50;
  }

  public function chechFullName($fullname) {
    $fullname = trim($fullname);
    return strlen($fullname) < 100;
  }

  public function signUp($email, $password, $fullname) {
    $query = "insert into users ( email, password, fullname) VALUES(:email, :password, :fullname)";
    if ($this->checkEmail($email)) {
      if ($this->checkPassword($password) == 1) {
        if ($this->chechFullName($fullname) == 1) {
          try {
            $result = $this->dbconn->prepare($query);
            $result->execute(array(':email'=>$email,
                                    ':password'=>$password,
                                    ':fullname'=>$fullname));
            return true;
          } catch(PDOException $e) {
            echo $e->getMessage();
            return false;
          }

        } else {
          return "invalid fullname";
        }
      } else {
        return "invalid password";
      }
    } else {
      return "invalid email";
    }
  }

  public function signIn($email, $password) {
    $userinfo = array();
    $query = "SELECT * FROM users WHERE email=:email AND password=:password";
    $stmt = $this->dbconn->prepare($query);
    try {
      $stmt->execute(array(':email'=>$email,
                           ':password'=>$password));
      while($row = $stmt->fetch()) {
        $userinfo['fullname'] = $row['fullname'];
        $userinfo['email'] = $row['email'];
        $userinfo['password'] = $row['password'];
        $userinfo['user_id'] = $row['user_id'];
        $userinfo['success'] = true;
        return $userinfo;
      }
    } catch(PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
    }
    return false;
  }

}

?>