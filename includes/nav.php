<!doctype html>
<html lang="en">
  <head>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <title>Welcome to LKeepr</title>
  </head>

  <body>
<nav class="navbar navbar-default">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">LKeepr</a>
      </div>
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
          <li class="active"><a href="#">Home</a></li>
          <li><a href="#">Lists</a></li>
        </ul>
        <?php
          if ( !isset($_SESSION['userinfo']) ) { ?>
          <form action="" class="navbar-form navbar-right" method="POST">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="username" name="username">
            </div>
            <div class="form-group">
              <input type="password" class="form-control" placeholder="password" name="password">
            </div>
            <button class="btn btn-default">Login</button>
            <a href="signup.php" class="btn btn-primary">Sign Up</a>
          </form>
        <?php } else { ?>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['userinfo']['fullname']; ?> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="profile.php">Profile</a></li>
            <li><a href="#">Test</a></li>
            <li><a href="#">Test</a></li>
            <li><a href="logout.php">Log out</a></li>
          </ul>
        </li>
      </ul>
        <?php } ?>

      </div>
    </nav>